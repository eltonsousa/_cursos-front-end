var nome = 'Elton';
var idade = 38;
var time = 'Flamengo';
console.log(nome, idade, time);

var valor = 5;
var qntd_item = 2;
var total = valor * qntd_item;
console.log(total);
 
// em uma linha
var nome = 'Elton', 
    sobrenome = 'Sousa', 
    altura = 170;
console.log(nome, sobrenome, altura);

// sem definir 
var semdefinir;
console.log(semdefinir);
 
// modificar variável Obs.: só mudam em: var e let, const não dá!
var time = 'Palmeiras';
var time = 'Flamengo';
console.log(time);

// let não aceita dois let repetidos
let time1 = 'Boa Vista';
    time1 = 'Iranduba';
console.log(time1);