// Declarar uma variável com o seu nome
var nome = 'Elton';

// Declarar uma variável com a sua idade
var idade = 38;

// Declarar uma variável com a sua comida
// favorita e não atribuir valor
var comidaFavorita;

// Atribuir valor a sua comida favorita
comidaFavorita = 'lasanha';

// Declarar 5 variáveis diferentes sem valores
var time, 
    profisssao,
    naturalidade, 
    cor, 
    religiao;

console.log(time, profisssao, naturalidade, cor, religiao);


