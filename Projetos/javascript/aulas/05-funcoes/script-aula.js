// Funçoes
// chamada de function declaration
function areaQuadrado(lado) {
  return lado * lado;
}

console.log(areaQuadrado(5)); // 5 é o argumento
// Parâmetros e Argumentos
/**************************************************
Ao criar uma função, você pode definir parâmetros.
Ao executar uma função, você pode passar argumentos.
***************************************************/

// peso e altura são os parâmetros
function imc(peso, altura) {
  const imc = peso / (altura * altura);
  return imc;
}

imc(80, 1.80) // 80 e 1.80 são os argumentos
imc(60, 1.70) // 60 e 1.70 são os argumentos

console.log(imc(76, 1.7));

/******************************************************
Separar por vírgula cada parâmetro.
Você pode definir mais de um parâmetro ou nenhum também
*******************************************************/

function corFavorita(cor) {
  if (cor === 'azul') {
    return 'Você gosta do céu';
  } else if (cor === 'verde') {
    return 'Você gosta de mato';
  } else {
    return 'Você não gosta de nada';
  }
}
console.log(corFavorita()); // retorna 'Você não gosta de nada'

// meus exemplos
function menu(opcao) {
  switch (opcao) {
    case '1':
      console.log('Comprar');
      break;
    case '2':
      console.log('Vender');
      break;
    case '3':
      console.log('Doar');
      break;
    default:
      console.log('escolha uma opção!')
      break;
  }
}
menu();

// ARGUMENTOS PODEM SER FUNÇÕES
/************************************************************
Chamadas de Callback, geralmente são funções que ocorrem 
após algum evento.
*************************************************************/
addEventListener('click', function () {
  console.log('Clicou');
});
/************************************************************
A função possui dois argumentos
Primeiro é a string 'click'
Segundo é uma função anônima
Funções anônimas são aquelas em que o nome da função 
não é definido, escritas como function() {} ou () => {}
*************************************************************/

// PODE OU NÃO RETORNAR UM VALOR
/************************************************************
Quando não definimos o return, ela irá retornar undefined.
O código interno da função é executado normalmente, 
independente de existir valor de return ou não.
************************************************************/
function imc2(peso, altura) {
  const imc = peso / (altura * altura);
  console.log(imc);
}

imc2(80, 1.80); // retorna o imc
console.log(imc2(80, 1.80)); // retorna o imc e undefined

// VALORES RETORNADOS
// Uma função pode retornar qualquer tipo de dado e até outras funções.
function terceiraIdade(idade) {
  if (typeof idade !== 'number') { // verifica qual o tipo de dado
    return 'Informe a sua idade!';
  } else if (idade >= 65) {
    return `é idoso`;
  } else {
    return `não é idoso`;
  }
}

console.log(terceiraIdade(14));
/*************************************************************
Cuidado, retornar diferentes tipos de dados na mesma 
função não é uma boa ideia.
************************************************************/

// ESCOPO
/*************************************************************
Variáveis e funções definidas dentro de um bloco {}, 
não são visíveis fora dele.
************************************************************/
// function precisoVisitar(paisesVisitados) {
//   var totalPaises = 193; // definido dentro do bloco
//   return `Ainda faltam ${totalPaises - paisesVisitados} paises para visitar`
// }
// console.log(totalPaises); // erro, totalPaises não definido

// O CORRETO SERIA:
var totalPaises1 = 193; // variavel fora
function precisoVisitar2(paisesVisitados2) {
  return `Ainda faltam ${totalPaises1 - paisesVisitados2} paises a visitar!`
}
console.log(precisoVisitar2(93));


// ESCOPO LÉXICO
/*************************************************************
Funções conseguem acessar variáveis que foram 
criadas no contexto pai
************************************************************/
var profissao = 'Web Designer';

function dados() {
  var nome = 'Elton Sousa';
  var idade = 38;
  function outrosDados() {
    var endereco = 'Manaus';
    var idade = 39;
    return `${nome}, ${idade}, ${endereco}, ${profissao}`; // Modelo template string
  }
  return outrosDados();
}

console.log(dados()); // Retorna 'Elton Sousa, 39, Manaus, Web Designer'
// outrosDados(); // retorna um erro
