// OBJETOS
/*******************************************************
Conjunto de variáveis e funções, que são chamadas 
de propriedades e métodos.
********************************************************/
var pessa = {
  nome: "Elton",
  idade: 38,
  profissao: "Designer",
  possuiFaculdade: true,
};
// Propriedades e métodos consistem em nome(chave) e valor

// MÉTODOS
/*******************************************************
É uma propriedade que possui uma função 
no local do seu valor.
********************************************************/
var quadrado = {
  lados: 4,
  area: function (lado) {
    return lado * lado;
  },
  perimetro: function (lado) {
    return this.lados * lado;
  },
};

quadrado.lados; // 4
quadrado.area(5); // 25
quadrado.perimetro(5); // 20
