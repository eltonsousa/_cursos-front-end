// Condicionais if else
var possuiSuperior = true;

if (possuiSuperior) {
  console.log("é verdadeiro");
} else {
  console.log("Não possui nada");
}

// Else if
var idade = 28;
var idade2 = 28;

if (idade > idade2) {
  console.log("é mais velho");
} else if (idade === idade2) {
  console.log("mesma idade");
} else {
  console.log("é mais novo!");
}

// Operador lógico de negação

if (!true) // false
  if (!1) // false
    if (!'') // true
      if (!undefined) // true

        // utiliza-se !! para verificar se a expressão é verdadeira ou falsa:
        if (!!' ') // true
          if (!!'') // false

            // Operadores de comparração
            10 == "10"; // true
10 == 10; // true
10 === "10"; // false
10 === 10; // true
10 != 15; // true
10 != "10"; // false
10 !== "10"; // true

/*
Operador lógico &&
compara se uma expresão e a outra é verdadeira.
retorna o primeiro valor FALSO ou o ultimo valor VERDADEIRO.
*/

true && true; // true
true && false; // false
"Gato" && "Cão" // "Cão"
5 - 5 && 5 + 5; // 0
"Gato" && false; // false


// Exemplo:
if (5 - 5 && 5 + 5) { // vai retornar o primeiro valor FALSO (5-5)
  console.log("verdadeiro");
} else {
  console.log("falso");
}

/* Operador lógico || `ou`
Retorna sempre o primeiro valor VERDADEIRO ou o ultimo valor FALSO.
*/

10 || 10; // 10
true || false; // true
false || true; // true
"Gato" || "Cão" // "Gato"
  (5 - 5) || (5 + 5); // 10
"Gato" || false; // "Gato"

// Exemplo
var condicional = 5 - 5 || 5 + 5 || 10 - 2; // retorna 10
console.log(condicional);

// Switch Case
var corFavorita = "amarelo";
switch (corFavorita) {
  case "azul":
    console.log("Olhe para o céu"); // executa comandos
    break; // para se for verdadeira
  case "vermelho":
    console.log("sou garantido");
    break;
  case "amarelo":
    console.log("Gosto de ouro");
    break;
  default:
    console.log("não gosta de cor");
    break;
}
