// Operadores aritméticos
var soma = 10 + 5; // 15
var sub = 10 - 5; // 5
var mult = 10 * 5; // 50
var div = 10 / 5; // 2

var expoente = 2 ** 4; // 16 = 2*2*2*2
var modulo = 14 % 5; // 4 resto da divisão

// Operadores Artméticos strings
var soma = "10" + 5; // 105
var sub = "10" - 5; // 5   consegue diminuir
var mult = "10" * "5"; // 50 consegue multiplicar
var div = "comprei 10" / 5; // NaN não é numero

// Ordem correta
// - Multiplicação e divisão depois some e subtração.
// - Parenteses para priorizar uma expressão.
var total1 = 20 + 5 * 2; // 30
var total2 = (20 + 5) * 2; // 50
var total3 = (20 / 2) * 5; // 30
var total4 = 10 + 10 * 2 + 20 / 2; // 30

// Operadores aritmeticos unarios
// unario de incremento
var i = 5;
console.log(i++); // ou i = i + 1
console.log(i);

var incremento2 = 5;
console.log(++incremento2);
