// Qual o resultado da seguinte expressão?

// Crie duas expressões que retornem NaN
var numero = 2;
var numero1 = "teste";
console.log(numero / numero1);

// Somar a string '200' com o número 50 e retornar 250
var num = +"200";
var num1 = 50;
console.log(num + num1);

// Incremente o número 5 e retorne o seu valor incrementado
var i = 5;
i++;
console.log(i);

// Como dividir o peso por 2?
var n1 = +"80" / 2;
var unidade = "kg";
var peso = n1 + unidade; // 80kg
console.log(peso);
