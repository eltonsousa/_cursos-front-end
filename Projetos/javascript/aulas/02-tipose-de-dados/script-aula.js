// verificar tipo de dados com typeof
var idade = 23;
console.log(typeof idade);

// somar string
var nome = 'Elton';
var sobrenome = 'Sousa'
var nomeCompleto = nome + ' ' + sobrenome;;
console.log(nomeCompleto);

// somar drting + number
var gols = '1000';
var frase = 'Romário fez ' + gols + ' gols';
console.log(frase);

// usando aspas
var exemplo1 = 'Java script é "super" fácil';
var exemplo2 = "Java script é 'super' fácil";
var exemplo3 = "Java script é \"super\" fácil";
var exemplo4 = `Java script é "super" fácil`;
console.log(exemplo4 );

// template string
var rua = 'Rua Malvuno Reis Neto';
var cep = '69039-680';
var bairro = 'Novo Israel';
var end = `${rua} ${cep} ${bairro}`;
console.log(end);

var gols = '1000';
//var frase = 'Romário fez ' + gols + ' gols';
var frase1 = `Romário fez ${gols} gols`;
console.log(frase1);
