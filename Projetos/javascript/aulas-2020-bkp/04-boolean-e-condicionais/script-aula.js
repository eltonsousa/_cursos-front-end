// var possuiGraduacao = true;

// if (possuiGraduacao) {
//   console.log("é verdadeiro");
// } else {
//   console.log("é falso");
// }

var possuiGraduacao = false;
var possuiDoutorado = true;

if (possuiGraduacao) {
  console.log("é verdadeiro");
} else if (possuiDoutorado) {
  console.log("Possui Doutorado");
} else {
  console.log("Não possui nada");
}
