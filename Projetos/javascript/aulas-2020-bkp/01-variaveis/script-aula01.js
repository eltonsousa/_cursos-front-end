// variaveis
var nome = "Elton",
  idade = 37,
  time = "Flamengo";

// mostra a o valor da variável
console.log(nome, idade, time);

//
var preco = 25,
  totalComprado = 5;

var totalPreco = totalComprado * preco;

console.log(totalPreco);

// Mudar valor da variável. var e let somente, const não é possível!!!
var time = "Vasco";
time = "Flamengo o melhor!";

console.log(time);
