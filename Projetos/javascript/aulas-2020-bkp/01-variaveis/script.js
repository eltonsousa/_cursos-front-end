// Declarar uma variável com o seu nome
var nome = "Elton";
// Declarar uma variável com a sua idade
var idade = 37;
// Declarar uma variável com a sua comida
// favorita e não atribuir valor
var comida;
// Atribuir valor a sua comida favorita
comida = "Ovo frito";
// Declarar 5 variáveis diferentes sem valores
var professor = "André",
  cidade = "Manaus",
  time = "Flamengo",
  curso = "JavaScript",
  sexo = "Masculino";

console.log(nome, idade, comida, professor, cidade, time, curso, sexo);
