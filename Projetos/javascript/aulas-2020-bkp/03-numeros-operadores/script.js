// Qual o resultado da seguinte expressão?
var total = 10 + (5 * 2) / 2 + 20; // 35
console.log(total);

// Crie duas expressões que retornem NaN
var num1 = "vinte e oito" / 2; // NaN
console.log(num1);

var num2 = "vinte e oito" + 2; // Usando 'soma' ele concatena
console.log(num2);

// Somar a string '200' com o número 50 e retornar 250
var num3 = +"200" + 50; // Usar mais(+) na frente pra tranformar em numero
console.log(num3);

// Incremente o número 5 e retorne o seu valor incrementado
var x = 5;
console.log(++x); // O mesmo que: x = x + 1

// Como dividir o peso por 2?
var numero = +"80" / 2;
var unidade = "kg";
var peso = numero + unidade;
console.log(peso);
