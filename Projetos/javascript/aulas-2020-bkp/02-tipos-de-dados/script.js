// declare uma variável contendo uma string
var nome = "Elton";

// declare uma variável contendo um numero dentro de uma string
var ano = "2020";

// declare uma variável com a sua idade
var idade = 37;

// declare duas variáveis, uma com seu nome e outra com seu sobrenome e some-as
var meuNome = "Elton";
var sobreNome = "Sousa";
var nomeCompleto = `${meuNome} ${sobreNome}`;

// coloque a seguinte frase em uma variavel: It's time
var frase = "it's time";

// verifique o tipo de variável que contém o seu nome
console.log(typeof nome);
