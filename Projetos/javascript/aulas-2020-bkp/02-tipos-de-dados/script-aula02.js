// Tipos de dados
var nome = "Elton"; // String
var idade = 37; // Number
var possuiFaculdade = true; // Boolean
var time; // Undefined
var comida = null; // Null
var simbolo = Symbol; // Symbol
var novoObjeto = {}; // Object

// verificar tipo de variável usando typeof
var nome = "Elton";
var idade = 37;
// console.log(typeof idade);

// somando strings
var nome = "Elton";
var sobrenome = "Sousa";
// console.log(nome + sobrenome);

var nome = "Elton";
var sobrenome = "Sousa";
// console.log(nome + " " + sobrenome);

//
var gols = 300;
var frase = "Gabigol fez " + gols + " gols";
// console.log(frase);

//
var ano = 2020;
var mes = 08;
var dia = 22;
var frase = "Hoje é " + dia + " do " + mes + " de " + ano;
// console.log(frase);

// Sempre usar aspas duplas e aspas simple ou vice versa ex.
var frase = "O Flamengo está muito 'forte'";
// console.log(frase);

//
var frase = 'O Flamengo está muito "forte"';
// console.log(frase);

// usando template string
var gols = 1000;
var frase3 = `Romario fez ${gols * 2} gols`;
console.log(frase3);
