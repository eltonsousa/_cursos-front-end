function areaQuadrado(lado) {
  return lado * lado;
}
console.log(areaQuadrado(4));

function imc(peso, altura) {
  // aqui é passado o parametro
  var imc = peso / (altura * altura);
  return imc;
}

console.log(imc(76, 1.7)); // aqui é passado o argumento
